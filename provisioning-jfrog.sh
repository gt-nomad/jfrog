#!/bin/bash
time ansible-playbook \
    -i ~/jfrog/ansible/inventory/localhost \
    -e @~/jfrog/ansible/env/vagrant-jfrog-vars.json \
    ~/jfrog/ansible/playbook/vagrant-jfrog.yml --flush-cache